function convertToHex (buffer) {
	while (buffer.charAt(0) === 0) {
				buffer = buffer.substr(1)
	}
	let hex = parseInt(buffer, 2).toString(16).toUpperCase();
	hex = hex.length === 1 ? '0x0'+hex : '0x'+hex;
	return hex;
}

function imageData2Hex (data) {
	let processed = [];
  let buffer = [];
	for (let pos = 0; pos < data.length; pos +=4) {
		buffer.push(data[pos+3] / 255);
		
		if (buffer.length === 8) {
			let hex = convertToHex(buffer.join(''));
    	processed.push(hex);
    	buffer = [];
    }
	}
	while(buffer.length === 8) {
		buffer.push(0);
		let hex = convertToHex(buffer.join(''));
    processed.push(hex);
	}
  return processed;
}
