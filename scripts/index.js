const c1 = document.querySelector("canvas#c1");
const c2 = document.querySelector("canvas#c2");
const ctx1 = c1.getContext("2d");
const ctx2 = c2.getContext("2d");
const charactersInput = document.querySelector("input#characters");
const sizeInput = document.querySelector("input#size");
const fontInputs = document.querySelectorAll('input[type=checkbox]');
const processButton = document.querySelector("input#process");
const downloadButton = document.querySelector("input#download");
const exampleDiv = document.querySelector(".example");

let procesedData;
let CHARACTERS;
let SIZE;
let FONT;

function init() {
    FONT = fontInputs[0].id;
    resizeCanvas();
    updateCharacters();
}

function resizeCanvas() {
    c2.width = exampleDiv.offsetWidth;
    c1.width = c2.width;
}

function updateFont(event) {
    fontInputs.forEach(check => check.checked = check === event.currentTarget);
    FONT = event.currentTarget.id;
    updateCharacters();
}

function updateCharacters() {
    procesedData = undefined;
    ctx1.clearRect(0, 0, c1.width, c1.height);
    ctx2.clearRect(0, 0, c2.width, c2.height);

    CHARACTERS = charactersInput.value;
    SIZE = sizeInput.value;

    exampleDiv.innerHTML = CHARACTERS;
    exampleDiv.style.fontSize = SIZE + "px";
    exampleDiv.style.fontFamily = FONT;
    downloadButton.disabled = true;
}

function getChar(ctx, txt) {
    ctx.clearRect(0, 0, c1.width, c1.height);
    ctx.clearRect(0, 0, c2.width, c2.height);
    ctx.fillText(txt, 0, 60);
    let charData = getCharData(ctx, 40);
    return (charData)
}

function drawContext2Char(ctx, charData, pos) {
    ctx.putImageData(charData.imageData, pos, 0);
}

function processCharacters() {
    let pos = 0;
    procesedData = CHARACTERS.split('').map(char => {
        ctx1.font = SIZE + "px " + FONT;
        let charData = getChar(ctx1, char);
        drawContext2Char(ctx2, charData, pos);
        pos += charData.w;
        let hexData = imageData2Hex(charData.imageData.data);
        return {
            data: hexData,
            char: char,
            width: charData.w,
            height: charData.h
        }
    });

    downloadButton.disabled = false;
}

processButton.addEventListener("click", processCharacters);
downloadButton.addEventListener("click", () => saveFile(procesedData, FONT, SIZE));
fontInputs.forEach(check => check.addEventListener("change", updateFont));
charactersInput.addEventListener("input", updateCharacters);
sizeInput.addEventListener("input", updateCharacters);
window.addEventListener("resize", resizeCanvas);

init();
