function camelize(str, pascalize = false) {
  return str.replace(/(?:^\w|[A-Z]|\b\w|\s+)/g, function(match, index) {
    if (+match === 0) return ""; // or if (/\s+/.test(match)) for white spaces
    return index == 0 && !pascalize ? match.toLowerCase() : match.toUpperCase();
  });
}

function getHeater (fontName) {
	return `
#include "${camelize(fontName, true)}.h"

#if (defined(__AVR__))
#include <avr\\pgmspace.h>
#else
#include <pgmspace.h>
#endif

`;
}
const LINE_BLOCKS = 100000;

function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);
  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

function saveFile(chars, fontName, fontSize) {
	let content = getHeater(fontName);
	let counter = 0;
	chars.forEach(char => {
		content += addChar(char.data, counter.toString(), char.width, char.height);
		counter ++;
	});

	content += `
extern int "${camelize(fontName)+ fontSize}Space = ${Math.floor(chars[0].height/10)};
	
extern sCHAR "${camelize(fontName)+ fontSize}[] = {`;
	counter = 0;
	chars.forEach(char => {
		content+= `
    {"${char.char}", Char${counter}_Table, ${char.width}, ${char.height}},`;
		counter ++;
	});

	content+= `
};
`;

	download(camelize(fontName, true) + fontSize + '.h', content);
}

function addChar(data, char, width, height) {
	let counter = 0
	let content = `const uint8_t Char${char}_Table[] PROGMEM = {`;
	data.forEach(hex => {
		content += hex+','
		counter ++
		if (counter === LINE_BLOCKS) {
			content += '\r\n'
			counter = 0
		}
	})
	content += `};
`;
	return content;
}
