function getCharData(ctx, minHeight, w=80, h=80) {
	let firstX;
	let firstY;
	let width;
	let height;
	let dataWidth;
	let dataHeight;

	let lastWhite = true;

	for (let x=0; x <w; x++) {
		let white = true;
		let y;
		for (y=0; y <h; y++) {
			if (ctx.getImageData(x, y, 1, 1).data[3] !== 0) {
				if (!firstX) {
					firstX = x;
				}
				white = false;
			}
		}
		//console.log(white, lastWhite)
		if (white && !lastWhite) {
			width = x-firstX;
		}
		lastWhite = white;
	}

	lastWhite = true;

	for (let y=0; y <h; y++) {
		let white = true;
		let x;
		for (x=0; x <w; x++) {
			if (ctx.getImageData(x, y, 1, 1).data[3]) {
				if (!firstY) {
					firstY = y;
				}
				white = false;
			}
		}
		if (white && !lastWhite) {
			height = y-firstY;
		}
		lastWhite = white;
	}

	if (minHeight > height) {
		firstY -= minHeight - height;
		height = minHeight;
	}
	dataWidth = width;
	dataHeight = height;

	while (dataWidth%8 !==0) dataWidth++;
	while (dataHeight%8 !==0) dataHeight++;

	return {
		x: firstX,
		y: firstY,
		w: width,
		h: height,
		dW: dataWidth,
		dH: dataHeight,
		imageData: ctx.getImageData(firstX, firstY, dataWidth, dataHeight)
	}
}
